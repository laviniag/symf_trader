<?php

namespace CarBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DefaultController extends Controller
{
    /**
     * @Route("/cars", name="offer")
     */
    public function indexAction(Request $request)
    {
        $car_repos = $this->getDoctrine()->getRepository('CarBundle:Car');
//        $cars = $car_repos->findAll();
        $cars = $car_repos->findCars();

        $form = $this->createFormBuilder()
            ->setMethod('GET')
            ->add('search', TextType::class, [
                'constraints' => [new NotBlank(), new Length(['min' => 2])]
            ])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            die('Form submitted');
        }
        return $this->render('CarBundle:Default:index.html.twig',
            ['cars' => $cars, 'form' => $form->createView()] );
    }

    /**
     * @Route("/car/{id}", name="scar")
     */
    public function showAction($id)
    {
        $car_repos = $this->getDoctrine()->getRepository('CarBundle:Car');
//        $car = $car_repos->find($id);
        $car = $car_repos->findById($id);
        return $this->render('CarBundle:Default:show.html.twig', ['car' => $car]);
    }
}
