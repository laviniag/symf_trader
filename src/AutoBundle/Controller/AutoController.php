<?php

namespace AutoBundle\Controller;

use AutoBundle\Entity\Auto;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Auto controller.
 *
 */
class AutoController extends Controller
{
    const   VIEW = 'AutoBundle:Auto:';
    /**
     * Lists all auto entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $autos = $em->getRepository('AutoBundle:Auto')->findAll();

        return $this->render(self::VIEW.'index.html.twig', array(
            'autos' => $autos,
        ));
    }

    /**
     * Creates a new auto entity.
     *
     */
    public function newAction(Request $request)
    {
        $auto = new Auto();
        $form = $this->createForm('AutoBundle\Form\AutoType', $auto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($auto);
            $em->flush();

            return $this->redirectToRoute('auto_show', array('id' => $auto->getId()));
        }

        return $this->render(self::VIEW.'new.html.twig', array(
            'auto' => $auto,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a auto entity.
     *
     */
    public function showAction(Auto $auto)
    {
        $deleteForm = $this->createDeleteForm($auto);

        return $this->render(self::VIEW.'show.html.twig', array(
            'auto' => $auto,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing auto entity.
     *
     */
    public function editAction(Request $request, Auto $auto)
    {
        $deleteForm = $this->createDeleteForm($auto);
        $editForm = $this->createForm('AutoBundle\Form\AutoType', $auto);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('auto_edit', array('id' => $auto->getId()));
        }

        return $this->render(self::VIEW.'edit.html.twig', array(
            'auto' => $auto,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a auto entity.
     *
     */
    public function deleteAction(Request $request, Auto $auto)
    {
        $form = $this->createDeleteForm($auto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($auto);
            $em->flush();
        }

        return $this->redirectToRoute('auto_index');
    }

    /**
     * Creates a form to delete a auto entity.
     *
     * @param Auto $auto The auto entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Auto $auto)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('auto_delete', array('id' => $auto->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
