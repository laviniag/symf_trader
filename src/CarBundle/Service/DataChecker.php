<?php

namespace CarBundle\Service;

use CarBundle\Entity\Car;

class DataChecker
{
    /** @var boolean */
    protected $require_images;

    /** @var EntityManager */
    protected $entity_manager;

    /**
     * DataChecker constructor.
     *
     * @param bool $entity_manager
     * @param bool $require_images
     */
    public function __construct($entity_manager, $require_images)
    {
        $this->entity_manager = $entity_manager;
        $this->require_images = $require_images;
    }

    public function checkCar(Car $car)
    {
        $promote = true;
        if ($this->require_images) {
            $promote = false;
        }

        $car->setPromote($promote);
        $this->entity_manager->persist($car);
        $this->entity_manager->flush();
        return $promote;
    }
}